import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { NgTerminal } from 'ng-terminal';
import { WebsocketService } from './services/websocket.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterViewInit{
  @ViewChild('term', { static: true }) term: NgTerminal

  constructor(private webSocket: WebsocketService) {}

  ngAfterViewInit() {
    // make websocket connection
    this.webSocket.connect().subscribe(msg => {
      console.log(msg)
      this.term.write(msg.data)
      // if (msg == '\r') {
      //   this.term.write('\r\n$');
      // }
      // this.term.write(msg)
    })

    this.term.keyEventInput.subscribe( e => {
      //console.log('keyboard event: ' + e.domEvent.keyCode + ', ' + e.key);
      const ev = e.domEvent;
      const printable = !ev.altKey && !ev.ctrlKey && !ev.metaKey;

      this.webSocket.send(e.key)
      // if (ev.keyCode === 13) {
      //   this.webSocket.send('\r\n', 0);
      //   //this.term.write('\r\n$');
      // } else if (ev.keyCode === 8) {
      //   // Do not delete the prompt
      //   if (this.term.underlying.buffer.active.cursorX > 2) {
      //     this.webSocket.send('\b \b');
      //     //this.term.write('\b \b');
      //   }
      // } else if (printable) {
      //   this.webSocket.send(e.key);
      //   //this.term.write(e.key);
      // }
    })
  }
}
