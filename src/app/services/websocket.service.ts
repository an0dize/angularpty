import { Injectable, NO_ERRORS_SCHEMA, OnDestroy } from "@angular/core";
import { Observable, of, pipe } from 'rxjs';
import { webSocket, WebSocketSubject } from 'rxjs/webSocket';
import { retryWhen, delay, switchMap } from 'rxjs/operators'

@Injectable()
export class WebsocketService implements OnDestroy {
  constructor() {}

  url: String = 'ws:localhost:10000'
  // Our connect() method returns an observable of the connection. 
  // You will call this method from your components to get an observable of all the data that comes through the WebSocket connection.
  connection$: WebSocketSubject<any>;
  RETRY_SECONDS = 10; 

  // connect2(): Observable<any> {
  //   return this.store.pipe(select(getApiUrl)).pipe(
  //     filter(apiUrl => !!apiUrl),
  //     // https becomes wws, http becomes ws
  //     map(apiUrl => apiUrl.replace(/^http/, 'ws') + '/stream'),
  //     switchMap(wsUrl => {
  //       if (this.connection$) {
  //         return this.connection$;
  //       } else {
  //         this.connection$ = webSocket(wsUrl);
  //         return this.connection$;
  //       }
  //     }),
  //     retryWhen((errors) => errors.pipe(delay(this.RETRY_SECONDS)))
  //   );
  // }


  connect(url?): Observable<any> {
    if (!url) {
      url = this.url;
    }
    return of(url).pipe(
      switchMap(wsUrl => {
        if (this.connection$) {
          return this.connection$;
        }
        else {
          this.connection$ = webSocket(wsUrl);
          return this.connection$;
        }
      }))//, retryWhen((errors) => { console.log(errors); return errors.pipe(delay(this.RETRY_SECONDS*1000))}))
  }

  // Our service will also include a send method. 
  // It works by just calling next() on the connection reference.
  // `stream`: 0=stdin, 1=stdout, 2=stderr.  For sending data from client, probably always going to be 0 for stdin
  send(data: any, stream: number = 0, auth?: any) {
    if (this.connection$) {
      const payload = {
        token: auth, 
        stream: stream,
        data: data,
      }
      //const payload = data
      this.connection$.next(payload);
    } else {
      console.error('Did not send data, open a connection first');
    }
  }

  closeConnection() {
    if (this.connection$) {
      this.connection$.complete();
      this.connection$ = null;
    }
  }
  ngOnDestroy() {
    this.closeConnection();
  }
}